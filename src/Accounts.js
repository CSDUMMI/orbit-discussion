class Account {
  constructor(id, name) {
    this.id = id
    this.name = name
    this.values = {}
  }
}

const put = {
  name: "put",
  apply(state, entry, { hash, identity }) {
    if(state[identity.id] !== undefined) {
      state[identity.id] = new Account(identity.id)
    }

    if(entry.key === "name") {
      state[idenity.id].name = entry.value
    } else {
      state[identity.id].values[entry.key] = entry.value
    }
    return state
  }
}

module.exports = {
  type: "accounts",
  operations: [put],
  initialState: {},
}
