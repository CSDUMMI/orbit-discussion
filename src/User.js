class User {
    /**
     * @param id of the user
     * @param firstActivity time of the first activity.
     */
    constructor(id, firstActivity) {
        this.id = id
        this.firstActivity = firstActivity
        this.activity = 0
        this.lastActivity = firstActivity
        this.comments = []
    }

    static MODACTIVITY = 50
    static VOTEACTIVITY = 25
    static PROPOSEACTIVITY = User.VOTEACTIVITY
    
    canMod() {
        return this.activity >= User.MODACTIVITY && this.lastActivity - this.firstActivity + 2>= User.MODACTIVITY
    }

    canPropose() {
        return this.activity >= User.PROPOSEACTIVITY && this.lastActivity - this.firstActivity + 2>= User.PROPOSEACTIVITY
    }

    canVote() {
        return this.activity >= User.VOTEACTIVITY && this.lastActivity - this.firstActivity + 2 >= User.VOTEACTIVITY
    }
}

module.exports = User
