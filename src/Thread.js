"use strict"
const Satellite = require("orbit-satellite")
const User = require("./User")
const Moderator = require("./Moderator")

const addComment = {
  name: "addComment",
  apply(state, text, { hash, identity, clock }) {
    state._comments[hash] = {
      text: text,
      id: identity.id,
      time: clock.time
    }

    if(state.users[identity.id] === undefined) {
      state._users[identity.id] = new User(identity.id, clock.time)
    }

    state._users[identity.id].activity++
    state._users[identity.id].comments.push(hash)
    state._users[identity.id].lastActivity = clock.time
    return state
  }
}

const removeComment = {
  name: "removeComment",
  apply(state, commentHash, { hash, identity, clock }) {
    let comment = state._comments[commentHash]
    let user = state._users[comment.id]
    
    if(comment.id === undefined || user === undefined) {
      return state
    }

    if(comment.id === identity.id || state.moderators.includes(identity.id)) {
      state._users[comment.id].comments.remove(commentHash)
      state._users[comment.id].activity--
      state._users[comment.id].lastActivity = comment.id === identity.id
        ? clock.time : state._users[comment.id].lastActivity
      delete state._comments[commentHash]
    }
  },
}

const proposeModerator = {
  name: "proposeModerator",
  apply(state, modId, { identity }) {
    if(state._users[identity.id].canPropose()
       && state_users[modId].canMod()) {
      state._moderators[modId] = new Moderator(modId, identity.id)
    }

    return state
  }
}

const voteModerator = {
  name: "voteModerator",
  apply(state, { modId, vote }, { identity }) {
    if(state._moderators[modId] !== undefined
       && state._users[identity.id].canVote()) {
      state._moderators[modId].vote(identity.id, vote)
    }
    return state
  },
}

module.exports = {
  type: "thread",
  operations: [addComment, removeComment, proposeModerator, voteModerator],
  initialState: (meta) => {
    return {
      _comments: {},
      _moderators: {},
      _users: {},

      get meta() { return meta },

      get moderators() {
        return Object
          .keys(this._moderators)
          .filter(id => {
            this._moderators[id].won()
          })
      }
    }
  },
}
