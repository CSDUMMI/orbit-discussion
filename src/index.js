const uuid = require("uuid")
const { CID } = require("multiformats/cid")

const Thread = require("./Thread")
const Threads = require("./Threads")
const Accounts = require("./Accounts")
const Satellite = require("orbit-satellite")



Satellite.addStore(Thread)
Satellite.addStore(Threads)
Satellite.addStore(Accounts)

module.exports = Satellite
module.exports.generateId = uuid.v4
module.exports.asCID = CID.asCID
