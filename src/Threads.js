const put = {
  name: "put",
  apply(state, address, { hash }) {
    state[hash] = address
    return state
  }
}

module.exports = {
  type: "threads",
  operations: [put],
  initialState: {}
}
