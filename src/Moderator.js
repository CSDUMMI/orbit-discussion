class Moderator {
    constructor(modId, proposer) {
        this._id = modId
        this._proposer = proposerId
        this._voters = {}
    }

    get id() { return this._id }
    
    vote(voterId, vote) {
        if(voter.canVote()) {
            this._voters[voterId] = vote
        }
    }

    won(users) {
        let userCount = Object.keys(users).length
        
        let votes = 0
        Object.keys(this._voters).forEach(voterId => {
            if(this._voters[voterId]) votes++
        })
        
        return votes/userCount > 0.5
    }
}

module.exports = Moderator
