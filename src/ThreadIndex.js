const Comment = require("./Comment")
const User = require("./User")
const Moderator = require("./Moderator")

class ThreadIndex {
    constructor() { this.clear() }
    
    clear() {
        this._comments = {}
        this._moderators = {}
        this._users = {}
    }

    get comments() {
        return Object.keys(this._comments).sort((a, b) => this._comments[a].time - this._comments[b].time
                                               ).map(c => this._comments[c])
    }

    get moderators() {
        return Object.values(this._moderators)
            .filter(mod => mod.won(this.users))
            .map(mod => mod.id)
    }

    get users() { return this._users }

    getUser(id) {
        return this._users[id]
    }
    
    addComment(hash, comment, id, time) {
        this._comments[hash] = new Comment(comment, id, time)

        if(this._users[id] === undefined) {
            this._users[id] = new User(id, time)
        }
        this._users[id].activity++
        this._users[id].comments.push(hash)
        this._users[id].lastActivity = time
        return true
    }

    removeComment(commentHash, id) {
        let comment = this._comments[commentHash]
        let user = this._users[comment.id]
        
        if(comment.id === id || this.moderators.includes(id)) {
            delete this._comments[commentHash]

            this._users[id].activity--
            this._users[id].comments = this._users[id].comments.filter(hash => hash !== commentHash)
            return true
        }
        return false
    }

    proposeModerator(proposedModId, proposerId) {
        let proposedMod = this._users[proposedModId]
        let proposer = this._users[proposerId]

        if(proposedMod !== undefined && proposer !== undefined
           && proposedMod.canMod() && proposer.canPropose()) {
            this._moderators[proposedModId] = new Moderator(proposedMod, proposer)
            return true
        }
        return false
    }

    voteModerator(voterId, vote, moderatorId) {
        let voter = this._users[voterId]
        let moderator = this._moderators[moderatorId]

        if(voter !== undefined && moderator !== undefined) {
            moderator.vote(voter, vote)
            return true
        }
        return false
    }
    
    applyEntry(entry) {
        switch(entry.payload.op) {
        case "ADDCOMMENT":
            this.addComment(entry.hash,entry.payload.comment, entry.identity.id, entry.clock.time)
            break
            
        case "REMOVECOMMENT":
            this.removeComment(entry.payload.commentHash, entry.identity.id)
            break
            
        case "PROPOSEMOD":
            this.proposeModerator(entry.payload.proposedMod, entry.identity.id)
            break
            
        case "VOTEMOD":
            this.voteModerator(entry.identity.id, entry.payload.vote, entry.payload.moderator)
            break
            
        default:
            console.log(`Couldn't apply: ${entry}`)
            break
        }
    }
    
    updateIndex(oplog) {
        this.clear()
        oplog.values
            .slice()
            .reduce((handled, item) => {
                if(!handled.includes(item.hash)) {
                    handled.push(item.hash)
                    try {
                        this.applyEntry(item)
                    } catch(e) {
                        console.log(e)
                    }
                }
                return handled
            }, [])
    }
}

module.exports = ThreadIndex
