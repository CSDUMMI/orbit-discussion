const FileType = require("file-type")
const uint8arrays = require("uint8arrays")
const {CID} = require("ipfs")

async function typeOf(ipfs,cid) {

  try {
    let chunks = []
    let type = undefined

    for await (const chunk of ipfs.cat(cid)) {
      chunks.push(chunk)
    }

    let data = uint8arrays.concat(chunks)
  
    return await FileType.fromBuffer(data)
  } catch(e) {
    return undefined
  }
}

module.exports = typeOf
