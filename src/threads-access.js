const AccessController = require('orbit-db-access-controllers/src/access-controller-interface')

class ThreadsAC extends AccessController {
    constructor(orbitdb, options) {
        super()
        this._orbitdb = orbitdb
        this._db = null
        this._options = options || {}
    }
    
    static get type() { return "threads" }

    async canAppend(entry, identityProvider) {
        
    }

    async close() {
        await this._db.close()
    }

    async load(address) {
        if(this._db) { await this._db.close() }

        this._db = await this._orbitdb.keyvalue(address, {
            accessController: {
                type: "ipfs",
                write: this._options.admin || [this._orbitdb.identity.id]
            },
            sync: true
        })

        await this._db.load()
    }
}
