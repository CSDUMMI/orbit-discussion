# orbit-discussion

This Project should allow users to create threads
on topics and discuss them.

It should also include features for moderation and
community management through the community.

## Thread 
A Thread discuss IPFS Objects referenced by CIDs.

Anyone can comment on any thread.
But Moderators can remove comments if they wan to.

Moderators are elected in an election of any 
user with more than VOTEACTIVITY Comments
and have to gain an absolute majority amongst 
them.

Once moderators loose this absolute majority,
their status is revoked.
