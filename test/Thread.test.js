const Satellite = require("orbit-satellite")
const Thread = require("../src/Thread")
const assert = require("assert")

describe("Thread", () => {
  let satellite, thread
  let i = 0
  
  before(async () => {
    Satellite.addStore(Thread)
    
    satellite = await Satellite.createInstance()
    thread = await satellite.create("exampleThread", "thread", {
      meta: {
        cid: "Qm...",
        topics: ["p2p", "ipfs", "orbitdb", "satellite", "discussion"],
      }
    })                               
  })

  it("Add comments", async () => {
    let hashes = []
    for(; i < 100; i++) {
      hashes.push(await thread.addComment(i.toString()))
    }

    hashes = hashes.map(h => h.hash)
    
    //assert.deepEqual(thread.state._users[satellite.orbitdb.identity.id].comments, hashes)
  })
})
