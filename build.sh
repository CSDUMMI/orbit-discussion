#!/bin/env bash

echo "Build started"
cp -r public/* dist
browserify src/index.js -s Satellite -o dist/bundle.js
echo "Build finished"

npm test && ipfs add --cid-version 1 -rn dist
