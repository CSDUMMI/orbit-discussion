const threadsAddr = "/orbitdb/zdpuAkyJ9Foya6D7GH9QcHQ7zhhpNrtwu9nKrRh4dNj4D4Ndw/orbit.discussion.main.threads"
const accountsAddr = "/orbitdb/zdpuAsbJJ6cX3d4duARJhQBmPWCsTBQ5Y2Yy2m6bVkuJj51kK/orbit.discussion.main.accounts"
      
/**
 * run orbit-discussion
 *
 * @param Satellite {Satellite} instance
 */
async function main (Satellite) {
  app = new Vue({
    el: '#app',
    data: {
      Satellite: Satellite,
      satellite: null,
      accounts: null,
      threads: null,
      message: "/? or /help for help",
      thread: null,
      threadId: null,
      threadCID: null,
      threadTopics: null,
      commands: [],
      ready: false,
      line: "",
    },
    async created() {
      Satellite.ipfsConfig.config.Addresses.Swarm.push(...[
        '/dns4/star.thedisco.zone/tcp/9090/wss/p2p-webrtc-star',
        '/dns6/star.thedisco.zone/tcp/9090/wss/p2p-webrtc-star',
        '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star',
      ])
      const satellite = await Satellite.createInstance()

      const accounts = await satellite.open(accountsAddr)
      const threads = await satellite.open(threadsAddr)

      console.log(accounts.address.toString())
      console.log(threads.address.toString())
  
      const commands = Commands(satellite, accounts)

      this.satellite = satellite
      this.accounts = accounts
      this.threads = threads
      this.commands = commands
      this.ready = true
    },
    methods: {
      async onInput() {
        let line = this.line

        if(this.hasCommand(line)) {
          this.runCommand(line)
        } else {
          this.comment(line)
        }
      },
      
      async runCommand(line) {
        const options = {
          app: app,
        }
        
        const commandName = line.split(" ")[0]
        
        let command = this.commands[commandName]
        let msg
        
        if(command.needsThread && this.thread) {
          msg = command.run(this.thread, line, options)
        } else {
          msg = command.run(undefined, line, options)
        }
        this.message = typeof msg === "string" ? msg : this.message
      },
    
      hasCommand(line) {
        const commandName = line.split(" ")[0]
        return this.commands[commandName] !== undefined
      },
      
      async comment(text) {
        if(this.thread) await this.thread.addComment(text)
      },
    }
  })

  
  
  return app

  function toCID (cid) {
    try {
      return IPFS.CID.parse(cid)
    } catch (e) {
      return false
    }
  }
}

async function initIPFS (ipfs) {
  const bootstrapNodes = [
    '/dns6/ipfs.thedisco.zone/tcp/4430/wss/p2p/12D3KooWChhhfGdB9GJy1GbhghAAKCUR99oCymMEVS4eUcEy67nt',
    '/dns4/ipfs.thedisco.zone/tcp/4430/wss/p2p/12D3KooWChhhfGdB9GJy1GbhghAAKCUR99oCymMEVS4eUcEy67nt'
  ]

  bootstrapNodes.forEach(async addr => {
    await ipfs.bootstrap.add(addr)
    await ipfs.swarm.connect(addr)
  })
}

main(Satellite)
