function Commands(satellite, accounts) {
  let commands = {
    "/proposemod": {
      run: async (thread, line) => {
        const parts = line.split(" ").filter(p => p != "")

        const identity = accounts.state.get(parts[1]).id

        if(thread.users[identity].canMod()
           && thread.users[satellite.identity.id].canPropose()) {
          await thread.proposeModerator(identity)
          await thread.addComment(`Proposed ${identity} for moderator`)
        }
      },
      help: "Propose a user for moderator<br>Usage: /proposemod &ltidentity|name&gt ",
      needsThread: true,
    },
    
    "/votemod": {
      run: async (thread, line) => {
        const parts = line.split(" ").filter(p => p != "")

        const vote = parts[1] == "for" || parts[1] == "+"
        const identity = accounts.state.get(parts[2]).id

        if(thread.users[orbitdb.identity.id].canVote()
           && thread._index._moderators[identity] !== undefined) {
          await thread.voteModerator(vote, identity)
          await thread.addComment(`Voted ${vote ? 'for' : 'against'} ${identity}`)
        }
      },
      help: "Vote for a moderator running for the role<br>Usage: /votemod &ltfor|against&gt &ltidentity|name&gt",
      needsThread: true,
    },
    "/setName": {
      run: async (thread, line) => {
        const parts = line.split(" ").filter(p => p != "")

        await accounts.put("name", parts[1])

        await thread.addComment(`Setting name to ${parts[1]}`)
      },
      help: "Set your username<br>Usage: /setName &ltusername&gt",
      needsThread: true,
    },
    "/moderators": {
      run: async (thread, line) => {
        const moderators = thread.moderators
              .map(id => accounts.state[id])
              .map(mod => mod["name"] ? mod["name"] : mod.id)
        
        await thread.addComment(`Current users with moderator status:<br>${moderators.join(',')}`)
      },
      help: "Display users with moderator status",
      needsThread: true,
    },
    "/create": {
      run: async (thread, line, { app }) => {
        const parts = line.split(" ").filter(p => p !== "")
        const cid = parts[1]
        const topics = parts.slice(2)

        if(app.Satellite.asCID(cid)) {
          await openThread(await satellite.create(app.Satellite.generateId(), "thread", {
            cid: cid,
            topics: topics,
          }), satellite, app)
        }
      },
      help: "Create a new thread on some topic.<br>Usage: /create &ltcid&gt &lttopics&gt",
      needsThread: false,
    },
    "/open": {
      run: async (thread, line, { app }) => {
        const parts = line.split(" ").filter(p => p !== "")
        const cid = parts[1]
        const topics = parts.slice(2)
        
        if(app.Satellite.asCID(cid)) {
          if(app.threads.state[cid]) {
            openThread(await satellite.open(app.threads.state[cid]), satellite, app)
          } else {
            app.message = `No Thread found for ${cid}. Open a new thread /create if you want to create a thread on this CID` 
          }
        }
      },
      help: "Open an existing thread.<br>Usage: /open &ltCID&gt",
    },
  }

  const commandsNames = Object.keys(commands)

  commands["/help"] = {
    run: (thread, line) => {
      const parts = line.split(" ").filter(p => p != "")

      if(parts.length == 1) {
        let msg = `<p><code>Available commands: ${commandsNames.join(",")}`

        if(!thread) {
          msg += `<br>Open a thread by pasting a CID`
        }

        msg += `</code></p>`
        return msg
      }
      else {
        let command = parts[1].startsWith("/") ? parts[1] : `/${parts[1]}`
        return commands[command].help
      }
    },
    help: "/help and /?",
    needsThread: false,
  }

  commands["/?"] = commands["/help"]
  return commands
}

async function openThread(thread, satellite, app) {
  app.threadId = await app.threads.put(thread.address.toString())
  app.threadCID = thread.state.meta.cid
  app.threadTopics = thread.state.meta.topics
  
  app.thread = thread
  app.message = `Thread ID: ${threadID}`
}
